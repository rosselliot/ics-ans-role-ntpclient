import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ntpd_group')


def test_ntpd_package(host):
    assert host.package("ntp").is_installed


def test_ntpd_conffile(host):
    f = host.file("/etc/ntp.conf")
    assert f.exists
    assert f.is_file


def test_ntpd_service(host):
    if 'debian' in host.ansible.get_variables()["inventory_hostname"]:
        s = host.service("ntp")
    else:
        s = host.service("ntpd")
    assert s.is_running
    assert s.is_enabled


def test_NOT_chrony_package(host):
    assert not host.package("chrony").is_installed
