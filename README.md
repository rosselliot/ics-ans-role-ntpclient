# ics-ans-role-ntpclient

Ansible role to install an ntpclient.
This role can install ntpd and chrony (... not at the same time).

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

```yaml
ntpclient_service_enable: true  # set to false to disable and remove the ntp daemons
ntpclient_servers:
  - 0.pool.ntp.org
  - 1.pool.ntp.org
ntpclient_timezone: Europe/Stockholm
ntpclient_use_chrony: true  # set to false to use ntpd
ntpclient_allow_from:
  - 10.10.10.0/24
  - 10.42.0.0/16
ntpclient_enable_chrony_log: false

```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-ntpclient
```

## License

BSD 2-clause
